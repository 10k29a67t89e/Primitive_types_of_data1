﻿using System;

namespace задание 
{
    class Program
    {
        static void Main(string[] args)
        {
            double x, y, z , f;
            x = double.Parse(Console.ReadLine());
            y = double.Parse(Console.ReadLine());
            z = double.Parse(Console.ReadLine());
            f = 1 / 33.0 + Math.Log(Math.Abs(y)) / 7 + z * 7 + Math.Min(x, 1 / 4.0);
            Console.WriteLine($"Переменная: {x}");
            Console.WriteLine($"Переменная: {y}");
            Console.WriteLine($"Переменная: {z}");
            Console.WriteLine($"Переменная: {f}");
        }
    }
}
