﻿using System;

namespace задание_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Даны стороны равнобедренной трапеции, найти все углы и длину диагонали
            int a, b, c;
            double d, A, B, S, D;
            Console.WriteLine($"Введите меньшее основание трапеции :");
            a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Введите большее основание трапеции :");
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Введите боковые стороны трапеции :");
            c = Convert.ToInt32(Console.ReadLine());
            //Диагональ
            d = (double)Math.Sqrt((c * c) + (a * b));
            //Углы
            A = ((a * a) + (c * c) - (d * d)) / 2 * a * c;
            S = Math.Cos(A);
            B = -((a * a) + (c * c) - (d * d)) / 2 * a * c;
            D = Math.Cos(B);
            Console.WriteLine($"Диагональ трипеции: {d}");
            Console.WriteLine($"Косинус угла А: {S}");
            Console.WriteLine($"Косинус угла В: {D}");
        }
    }
}
