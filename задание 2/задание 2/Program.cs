﻿using System;

namespace задание_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double x,f;
            x = double.Parse(Console.ReadLine());
            f = (1 / 33.0) + Math.Log(Math.Abs(x)) / 7 + 7 * x + Math.Min(x, 1 / 4.0) + Math.Cos(x / 3) + Math.Exp((x/3)*Math.Log(x))/3;
            Console.WriteLine($"Переменная: {x}");
            Console.WriteLine($"Переменная: {f}");
        }
    }
}
